package cz.pavelzelenka.game;

import javafx.application.Application;
import javafx.embed.swing.SwingNode;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Okno aplikace
 * @author Pavel Zelenka A16B0176P
 * @version 2017-05-13
 */

public class GameWindow extends Application {

	enum Mode {
		Game,
		Graph;
	}
	
	private Mode mode = Mode.Game;
	
	private static GameWindow instance;
	private static Game game;
	
	private StackPane stackPane = new StackPane();
	private VBox centerVBox = new VBox();
	private HBox graphMenuHBox = new HBox();
	private SwingNode swingNode = new SwingNode();
	
	private AnchorPane rightPane;
	private AnchorPane bottomPane;
	
	private TextField azimuthField = new TextField();
	private TextField elevationField = new TextField();
	private TextField velocityField = new TextField();
	private Button shootButton = new Button("Shoot");
	private Text azimuthText = new Text("Azimuth:");
	private Text elevationText = new Text("Elevation:");
	private Text velocityText = new Text("Start velocity:");
	
	private Button gameButton, windButton, changeMapButton, mapInfoButton, graphsButton, exitButton;
	private Button rangeButton, terrainProfileButton;
	
	private int result;
	
	private static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		instance = this;
		stage.setTitle("UPG | Game | A16B0176P");
		stage.setScene(getScene());
		stage.setMinHeight(500);
		stage.setMinWidth(500);
		stage.show();
		stage.setHeight(stage.getMinHeight());
		stage.setWidth(stage.getMinWidth());
	}

	private Scene getScene() {
		Scene scene = new Scene(initRootLayout());
		return scene;
	}

	private Parent initRootLayout() {
		BorderPane borderPane = new BorderPane();
		
		borderPane.setCenter(getCenterPane());
		borderPane.setRight(getRightPane());
		borderPane.setBottom(getBottomPane());
		
		return borderPane;
	}

	/**
	 * Vrati stredni panel
	 * @return stredni panel
	 */
	private Node getCenterPane() {
		AnchorPane anchorPane = new AnchorPane();
		anchorPane.setStyle("-fx-background-color: white;");
		swingNode.setContent(Game.gamePanel);
		centerVBox.getChildren().add(swingNode);
		VBox.setVgrow(swingNode, Priority.ALWAYS);
		stackPane.getChildren().add(centerVBox);
		anchorPane.getChildren().add(stackPane);
		AnchorPane.setTopAnchor(stackPane, 0D);
	    AnchorPane.setLeftAnchor(stackPane, 0D);
	    AnchorPane.setRightAnchor(stackPane, 0D);
	    AnchorPane.setBottomAnchor(stackPane, 0D);
	    initGraphMenuPanel();
		return anchorPane;
	}
	
	private void initAzimuthTextField() {
		azimuthField.setText("0.0");
		azimuthField.setPromptText("0.0");
		azimuthField.textProperty().addListener((observable, oldvalue, newValue) -> {
			try{
				Double.parseDouble(newValue + "0");
			} catch(Exception e) {
				azimuthField.setText(oldvalue);
			}
			if(mode.equals(Mode.Graph)) {
				showGraph();
			}
		});
	}
	
	private void initElevationField() {
		elevationField.setText("0.0");
		elevationField.setPromptText("0.0");
		elevationField.textProperty().addListener((observable, oldvalue, newValue) -> {
			try{
				Double.parseDouble(newValue + "0");
			} catch(Exception e) {
				elevationField.setText(oldvalue);
			}
			if(mode.equals(Mode.Graph)) {
				showGraph();
			}
		});
	}
	
	private void initVelocityField() {
		velocityField.setText("0.0");
		velocityField.setPromptText("0.0");
		velocityField.textProperty().addListener((observable, oldvalue, newValue) -> {
			try{
				Double.parseDouble(newValue + "0");
			} catch(Exception e) {
				velocityField.setText(oldvalue);
			}
			if(mode.equals(Mode.Graph)) {
				showGraph();
			}
		});
	}
	
	private void initGraphMenuPanel() {
		rangeButton = new Button("RANGE");
		rangeButton.setOnAction(event -> {
			game.graphController.showRangeChart();
			swingNode.setContent(game.graphController.getPanel());
		});
		rangeButton.setPrefWidth(100D);
		rangeButton.setMaxWidth(Double.MAX_VALUE);
		setDarkStyle(rangeButton);
		
		terrainProfileButton = new Button("TERRAIN PROFILE");
		terrainProfileButton.setOnAction(event -> {
			game.graphController.showTerrainProfileChart();;
			swingNode.setContent(game.graphController.getPanel());
		});
		terrainProfileButton.setPrefWidth(100D);
		terrainProfileButton.setMaxWidth(Double.MAX_VALUE);
		setDarkStyle(terrainProfileButton);
		
		HBox.setHgrow(rangeButton, Priority.ALWAYS);
		HBox.setHgrow(terrainProfileButton, Priority.ALWAYS);
		graphMenuHBox.getChildren().addAll(rangeButton, terrainProfileButton);
		graphMenuHBox.setPadding(new Insets(5D, 5D, 5D, 5D));
		graphMenuHBox.setSpacing(5D);
	}
	
	/**
	 * Vrati stredni panel
	 * @return stredni panel
	 */
	private Node getRightPane() {
		rightPane = new AnchorPane();
		rightPane.setStyle("-fx-background-color: white;");
		VBox vBox = new VBox();
		
		gameButton = new Button("GAME");
		gameButton.setOnAction(event -> showGame());
		gameButton.setMaxWidth(Double.MAX_VALUE);
		setDarkStyle(gameButton);
		gameButton.setDisable(true);
		
		windButton = new Button("WIND");
		windButton.setOnAction(event -> changeWind());
		windButton.setMaxWidth(Double.MAX_VALUE);
		setDarkStyle(windButton);
		
		changeMapButton = new Button("CHANGE MAP");
		changeMapButton.setOnAction(event -> changeMap());
		changeMapButton.setMaxWidth(Double.MAX_VALUE);
		setDarkStyle(changeMapButton);
		
		mapInfoButton = new Button("MAP INFO");
		mapInfoButton.setOnAction(event -> showMapInfo());
		mapInfoButton.setMaxWidth(Double.MAX_VALUE);
		setDarkStyle(mapInfoButton);
		
		graphsButton = new Button("GRAPHS");
		graphsButton.setOnAction(event -> showGraph());
		graphsButton.setMaxWidth(Double.MAX_VALUE);
		setDarkStyle(graphsButton);
		
		exitButton = new Button("EXIT");
		exitButton.setOnAction(event -> Game.exit());
		exitButton.setMaxWidth(Double.MAX_VALUE);
		exitButton.setStyle("-fx-base: #ED1C23;"
				+ "-fx-text-fill: #242424;"
				+ "-fx-font-weight: bold;");
		
		vBox.getChildren().addAll(gameButton, windButton, changeMapButton, mapInfoButton, graphsButton, exitButton);
		vBox.setSpacing(5D);
		rightPane.getChildren().add(vBox);
		AnchorPane.setTopAnchor(vBox, 5D);
	    AnchorPane.setLeftAnchor(vBox, 5D);
	    AnchorPane.setRightAnchor(vBox, 5D);
	    AnchorPane.setBottomAnchor(vBox, 5D);
		return rightPane;
	}
	
	/**
	 * Vrati dolni panel
	 * @return dolni panel
	 */
	private Node getBottomPane() {
		bottomPane = new AnchorPane();
		bottomPane.setStyle("-fx-background-color: white;");
		
		GridPane gridPane = new GridPane();
		gridPane.setVgap(5D);
		gridPane.setHgap(5D);
		gridPane.setAlignment(Pos.CENTER);
		
		gridPane.add(azimuthText, 0, 0, 1, 1);
		azimuthField.setPrefWidth(100);
		gridPane.add(azimuthField, 0, 1, 1, 1);
		initAzimuthTextField();
		
		gridPane.add(elevationText, 1, 0, 1, 1);
		elevationField.setPrefWidth(100);
		gridPane.add(elevationField, 1, 1, 1, 1);
		initElevationField();

		gridPane.add(velocityText, 2, 0, 1, 1);
		velocityField.setPrefWidth(100);
		gridPane.add(velocityField, 2, 1, 1, 1);
		initVelocityField();
		
		shootButton.setPrefWidth(100);
		shootButton.setMaxHeight(Double.MAX_VALUE);
		gridPane.add(shootButton, 3, 0, 1, 2);
		shootButton.setOnAction(event -> shoot());
		setDarkStyle(shootButton);
		
		bottomPane.getChildren().add(gridPane);
		AnchorPane.setTopAnchor(gridPane, 7D);
	    AnchorPane.setLeftAnchor(gridPane, 7D);
	    AnchorPane.setRightAnchor(gridPane, 7D);
	    AnchorPane.setBottomAnchor(gridPane, 7D);
		return bottomPane;
	}
	
	private void showMapInfo() {
		disableMenus(true);
		VBox vBox = new VBox();
		vBox.setStyle("-fx-background-color: transparent;");
		vBox.setAlignment(Pos.TOP_CENTER);
		vBox.setSpacing(5D);
		vBox.setPadding(new Insets(10D));
		
		DropShadow ds = new DropShadow();
		ds.setOffsetY(3.0f);
		ds.setColor(Color.color(0.4f, 0.4f, 0.4f));
		
		Text text = new Text("Map Info");
		text.setFont(Font.font("Verdana", FontWeight.BOLD, 24));
		text.setStrokeWidth(1.5D);
		text.setStroke(Color.WHITE);
		text.setEffect(ds);
		vBox.getChildren().addAll(text);
		
		TextArea textArea = new TextArea();
		textArea.setText(game.getGameObjectInfo());
		textArea.setPrefColumnCount(150);
		textArea.setEditable(false);
		textArea.setWrapText(true);
		textArea.setMaxWidth(300D);
		vBox.getChildren().add(textArea);
		
		Button cancelButton = new Button("Cancel");
		cancelButton.setMaxWidth(150D);
		cancelButton.setStyle("-fx-base: #FF6600;"
				+ "-fx-text-fill: white;"
				+ "-fx-font-weight: bold;");
		cancelButton.setEffect(ds);
		cancelButton.setOnAction(action -> {
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBox.getChildren().add(cancelButton);
		
		stackPane.getChildren().add(vBox);
		StackPane.setAlignment(vBox, Pos.CENTER);
	}
	
	private void changeMap() {
		disableMenus(true);
		VBox vBox = new VBox();
		vBox.setStyle("-fx-background-color: transparent;");
		vBox.setAlignment(Pos.TOP_CENTER);
		vBox.setSpacing(5D);
		vBox.setPadding(new Insets(10D));
		
		HBox hBox = new HBox();
		hBox.setAlignment(Pos.TOP_CENTER);
		hBox.setSpacing(5D);
		VBox vBoxR = new VBox();
		vBoxR.setAlignment(Pos.TOP_CENTER);
		vBoxR.setSpacing(5D);
		VBox vBoxL = new VBox();
		vBoxL.setAlignment(Pos.TOP_CENTER);
		vBoxL.setSpacing(5D);
		hBox.getChildren().addAll(vBoxL, vBoxR);
		
		DropShadow ds = new DropShadow();
		ds.setOffsetY(3.0f);
		ds.setColor(Color.color(0.4f, 0.4f, 0.4f));
		
		Text text = new Text("Change Map");
		text.setFont(Font.font("Verdana", FontWeight.BOLD, 24));
		text.setStrokeWidth(1.5D);
		text.setStroke(Color.WHITE);
		text.setEffect(ds);
		vBox.getChildren().addAll(text, hBox);
		
		Button randomButton = new Button("Random");
		randomButton.setPrefWidth(150D);
		setDarkStyle(randomButton);
		randomButton.setEffect(ds);
		randomButton.setOnAction(action -> {
			game.changeMap(false);
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBoxL.getChildren().add(randomButton);
		
		Button fromFileButton = new Button("From File");
		fromFileButton.setPrefWidth(150D);
		setDarkStyle(fromFileButton);
		fromFileButton.setEffect(ds);
		fromFileButton.setOnAction(action -> {
			game.changeMap(true);
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBoxR.getChildren().add(fromFileButton);
		
		Button cancelButton = new Button("Cancel");
		cancelButton.setPrefWidth(150D);
		cancelButton.setStyle("-fx-base: #FF6600;"
				+ "-fx-text-fill: white;"
				+ "-fx-font-weight: bold;");
		cancelButton.setEffect(ds);
		cancelButton.setOnAction(action -> {
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBoxR.getChildren().add(cancelButton);
		
		stackPane.getChildren().add(vBox);
		StackPane.setAlignment(vBox, Pos.CENTER);
	}
	
	private void changeWind() {
		disableMenus(true);
		VBox vBox = new VBox();
		vBox.setStyle("-fx-background-color: transparent;");
		vBox.setAlignment(Pos.TOP_CENTER);
		vBox.setSpacing(5D);
		vBox.setPadding(new Insets(10D));
		
		HBox hBox = new HBox();
		hBox.setAlignment(Pos.TOP_CENTER);
		hBox.setSpacing(5D);
		VBox vBoxR = new VBox();
		vBoxR.setAlignment(Pos.TOP_CENTER);
		vBoxR.setSpacing(5D);
		VBox vBoxL = new VBox();
		vBoxL.setAlignment(Pos.TOP_CENTER);
		vBoxL.setSpacing(5D);
		hBox.getChildren().addAll(vBoxL, vBoxR);
		
		DropShadow ds = new DropShadow();
		ds.setOffsetY(3.0f);
		ds.setColor(Color.color(0.4f, 0.4f, 0.4f));
		
		Text text = new Text("Wind");
		text.setFont(Font.font("Verdana", FontWeight.BOLD, 24));
		text.setStrokeWidth(1.5D);
		text.setStroke(Color.WHITE);
		text.setEffect(ds);
		vBox.getChildren().addAll(text, hBox);
		
		Button calmButton = new Button("Calm");
		calmButton.setPrefWidth(150D);
		setDarkStyle(calmButton);
		calmButton.setEffect(ds);
		calmButton.setOnAction(action -> {
			game.setCalmWind();
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBoxL.getChildren().add(calmButton);
		
		Button lightAirButton = new Button("Light Air");
		lightAirButton.setPrefWidth(150D);
		setDarkStyle(lightAirButton);
		lightAirButton.setEffect(ds);
		lightAirButton.setOnAction(action -> {
			game.setLightAirWind();
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBoxL.getChildren().add(lightAirButton);
		
		Button gentleBreeze = new Button("Gentle Breeze");
		gentleBreeze.setPrefWidth(150D);
		setDarkStyle(gentleBreeze);
		gentleBreeze.setEffect(ds);
		gentleBreeze.setOnAction(action -> {
			game.setGentleBreezeWind();
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBoxL.getChildren().add(gentleBreeze);
		
		Button strongBreeze = new Button("Strong Breeze");
		strongBreeze.setPrefWidth(150D);
		setDarkStyle(strongBreeze);
		strongBreeze.setEffect(ds);
		strongBreeze.setOnAction(action -> {
			game.setStrongBreezeWind();
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBoxL.getChildren().add(strongBreeze);
		
		Button highWindButton = new Button("High Wind");
		highWindButton.setPrefWidth(150D);
		setDarkStyle(highWindButton);
		highWindButton.setEffect(ds);
		highWindButton.setOnAction(action -> {
			game.setHighWind();
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBoxL.getChildren().add(highWindButton);
		
		Button galeButton = new Button("Gale");
		galeButton.setPrefWidth(150D);
		setDarkStyle(galeButton);
		galeButton.setEffect(ds);
		galeButton.setOnAction(action -> {
			game.setGaleWind();
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBoxL.getChildren().add(galeButton);
		
		Button strongGaleButton = new Button("Strong Gale");
		strongGaleButton.setPrefWidth(150D);
		setDarkStyle(strongGaleButton);
		strongGaleButton.setEffect(ds);
		strongGaleButton.setOnAction(action -> {
			game.setStrongGaleWind();
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBoxL.getChildren().add(strongGaleButton);
		
		Button stormButton = new Button("Storm");
		stormButton.setPrefWidth(150D);
		setDarkStyle(stormButton);
		stormButton.setEffect(ds);
		stormButton.setOnAction(action -> {
			game.setStormWind();
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBoxL.getChildren().add(stormButton);
		
		Button violentStormButton = new Button("Violent Storm");
		violentStormButton.setPrefWidth(150D);
		setDarkStyle(violentStormButton);
		violentStormButton.setEffect(ds);
		violentStormButton.setOnAction(action -> {
			game.setViolentStormWind();
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBoxL.getChildren().add(violentStormButton);
		
		Button hurricaneButton = new Button("Hurricane");
		hurricaneButton.setPrefWidth(150D);
		setDarkStyle(hurricaneButton);
		hurricaneButton.setEffect(ds);
		hurricaneButton.setOnAction(action -> {
			game.setHurricaneWind();
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBoxL.getChildren().add(hurricaneButton);
		
		Button southWindButton = new Button("South Wind");
		southWindButton.setPrefWidth(150D);
		setDarkStyle(southWindButton);
		southWindButton.setEffect(ds);
		southWindButton.setOnAction(action -> {
			game.setSouthWind();
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBoxR.getChildren().add(southWindButton);
		
		Button northWindButton = new Button("North Wind");
		northWindButton.setPrefWidth(150D);
		setDarkStyle(northWindButton);
		northWindButton.setEffect(ds);
		northWindButton.setOnAction(action -> {
			game.setNorthWind();
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBoxR.getChildren().add(northWindButton);
		
		Button eastWindButton = new Button("East Wind");
		eastWindButton.setPrefWidth(150D);
		setDarkStyle(eastWindButton);
		eastWindButton.setEffect(ds);
		eastWindButton.setOnAction(action -> {
			game.setEastWind();
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBoxR.getChildren().add(eastWindButton);
		
		Button westWindButton = new Button("West Wind");
		westWindButton.setPrefWidth(150D);
		setDarkStyle(westWindButton);
		westWindButton.setEffect(ds);
		westWindButton.setOnAction(action -> {
			game.setWestWind();
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBoxR.getChildren().add(westWindButton);
		
		Button cancelButton = new Button("Cancel");
		cancelButton.setPrefWidth(150D);
		cancelButton.setStyle("-fx-base: #FF6600;"
				+ "-fx-text-fill: white;"
				+ "-fx-font-weight: bold;");
		cancelButton.setEffect(ds);
		cancelButton.setOnAction(action -> {
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBoxR.getChildren().add(cancelButton);
		
		stackPane.getChildren().add(vBox);
		StackPane.setAlignment(vBox, Pos.CENTER);
	}
	
	private void showGraph() {
		if(!centerVBox.getChildren().contains(graphMenuHBox)) {
			centerVBox.getChildren().add(0, graphMenuHBox);
		}
		this.mode = Mode.Graph;
		this.swingNode.setContent(game.graphController.getPanel());
		double a = getAzimuth();
		double e = getElevation();
		double v = getVelocity();
		this.game.graphController.setParams(a,e,v);
		this.windButton.setDisable(true);
		this.gameButton.setDisable(false);
		this.graphsButton.setDisable(true);
		this.changeMapButton.setDisable(true);
		this.shootButton.setDisable(true);
	}
	
	private void showGame() {
		if(centerVBox.getChildren().contains(graphMenuHBox)) {
			centerVBox.getChildren().remove(graphMenuHBox);
		}
		this.mode = Mode.Game;
		this.swingNode.setContent(game.gamePanel);
		this.gameButton.setDisable(true);
		this.windButton.setDisable(false);
		this.graphsButton.setDisable(false);
		this.changeMapButton.setDisable(false);
		this.shootButton.setDisable(false);
	}
	
	private void showResult() {
		disableMenus(true);
		VBox vBox = new VBox();
		vBox.setStyle("-fx-background-color: transparent;");
		vBox.setAlignment(Pos.TOP_CENTER);
		vBox.setSpacing(5D);
		vBox.setPadding(new Insets(10D));
		
		DropShadow ds = new DropShadow();
		ds.setOffsetY(3.0f);
		ds.setColor(Color.color(0.4f, 0.4f, 0.4f));
		
		Text text = new Text("Result");
		text.setFont(Font.font("Verdana", FontWeight.BOLD, 24));
		text.setStrokeWidth(1.5D);
		text.setStroke(Color.WHITE);
		text.setEffect(ds);
		vBox.getChildren().addAll(text);
		
		Button cancelButton = new Button("Damn it!");
		cancelButton.setMaxWidth(150D);
		cancelButton.setStyle("-fx-base: #FF6600;"
				+ "-fx-text-fill: white;"
				+ "-fx-font-weight: bold;");
		cancelButton.setEffect(ds);
		cancelButton.setOnAction(action -> {
			stackPane.getChildren().remove(vBox);
			disableMenus(false);
		});
		vBox.getChildren().add(cancelButton);
		
		
		switch(result) {
			case -1: text.setText("Shot Missed the Target!");
				break;
			case 0: text.setText("Out of the Map!");
				break;
			case 1: text.setText("Hit!");
				cancelButton.setText("Great!");
				cancelButton.setStyle("-fx-base: #17AA01;"
						+ "-fx-text-fill: white;"
						+ "-fx-font-weight: bold;");
				break;
			default: text.setText("Shot Missed the Target");
				break;
		}
		
		stackPane.getChildren().add(vBox);
		StackPane.setAlignment(vBox, Pos.CENTER);
	}
	
	private void disableMenus(boolean value) {
		rightPane.setDisable(value);
		bottomPane.setDisable(value);
		rangeButton.setDisable(value);
		terrainProfileButton.setDisable(value);
	}
	
	private double getAzimuth() {
		double a = 0;
		try {
			a = Double.parseDouble(azimuthField.getText());
		} catch(Exception exception) {
			a = 0D;
		}
		return a;
	}
	
	private double getElevation() {
		double e = 0;
		try {
			e = Double.parseDouble(elevationField.getText());
		} catch(Exception exception) {
			e = 0D;
		}
		return e;
	}
	
	private double getVelocity() {
		double v = 0;
		try {
			v = Double.parseDouble(velocityField.getText());
		} catch(Exception exception) {
			v = 0D;
		}
		return v;
	}
	
	private void setDarkStyle(Node node) {
		node.setStyle("-fx-base: #232629;"
				+ "-fx-text-fill: white;"
				+ "-fx-font-weight: bold;");
	}
	
	private void shoot() {
		double a = getAzimuth();
		double e = getElevation();
		double v = getVelocity();
		game.shootingCalculator.shoot(a, e, v);
		this.result = game.getResult();
		showResult();
	}
	
	public Game getGame() {
		return game;
	}

	public void setGame(Game g) {
		game = g;
	}

	public static GameWindow getInstance(String[] args, Game g) {
		game = g;
		main(args);
		return instance;
	}
	
}
