package cz.pavelzelenka.game;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

/**
 * Zobrazeni radaru
 * @author Pavel Zelenka A16B0176P
 * @version 2017-04-11
 */

public class Radar {

	private Game game;
	private GamePanel gamePanel;
	private Terrain terrain;
	private Wind wind;
	private NamedPosition target, shooter;
	private BufferedImage radarImage;
	
	public Radar(Game game, GamePanel gamePanel, Terrain terrain, Wind wind) {
		this.game = game;
		this.gamePanel = gamePanel;
		this.terrain = terrain;
		this.wind = wind;
	}
	
	/**
	 * Metoda pro zobrazeni grafickeho kontextu
	 * @param g2 Graphics2D
	 * @param scale meritko
	 */
	public void draw(Graphics2D g2, double scale) {
		makeImage();
		
		AffineTransform transform = g2.getTransform();
		g2.setClip(null);
		
		AffineTransform affineTransform = AffineTransform.getScaleInstance(1,1);
		
		AffineTransformOp op = new AffineTransformOp(affineTransform, AffineTransformOp.TYPE_BICUBIC);
		
		g2.drawImage(radarImage, op, 0, 0);
		
		g2.setTransform(transform);
	}
	
	/**
	 * Vytvoreni obrazku radar
	 */
	public void makeImage() {
		this.radarImage = new BufferedImage(150, 200, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = radarImage.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		Ellipse2D ellipse = new Ellipse2D.Double(0,0, 100,100);
		
		Font font = new Font("Calibri", Font.BOLD, 12);
		g2.setFont(font);
		
		g2.translate(font.getSize()+10, font.getSize()+10);
		
		g2.setColor(Color.BLACK);
		String wind = "Wind";
		int windWidth = g2.getFontMetrics().stringWidth(wind);
		g2.drawString(wind, (int)((ellipse.getWidth()/2)-(windWidth/2)), 0);
		
		g2.translate(0, font.getSize()+10);
		
		int nWidth = g2.getFontMetrics().stringWidth("N");
		int eWidth = g2.getFontMetrics().stringWidth("E");
		int sWidth = g2.getFontMetrics().stringWidth("S");
		int wWidth = g2.getFontMetrics().stringWidth("W");
		g2.setColor(Color.RED);
		g2.drawString("N", (int)((ellipse.getWidth()/2)-(nWidth/2)), -5);
		g2.setColor(Color.BLACK);
		g2.drawString("E", (int)((ellipse.getWidth()+5)), (int)(ellipse.getHeight()/2)+(font.getSize()/2));
		g2.drawString("S", (int)((ellipse.getWidth()/2)-(sWidth/2)), (int)(ellipse.getHeight()+font.getSize()+5));
		g2.drawString("W", -(5+wWidth), (int)(ellipse.getHeight()/2)+(font.getSize()/2));
		String velocityInfo = String.format("%.0f", Wind.convertMpsToKmph(this.wind.getVelocity())) + " km/h";
		int velocityWidth = g2.getFontMetrics().stringWidth(velocityInfo);
		g2.drawString(velocityInfo, (int)((ellipse.getWidth()/2)-(velocityWidth/2)), (int)(ellipse.getHeight()+2*font.getSize()+10));
		
		GradientPaint gradient = new GradientPaint(0,0,new Color(244, 244, 244),50, 0,new Color(175, 175, 175), false);
		g2.setPaint(gradient);
		g2.fill(ellipse);
		
		g2.translate(50, 50);
		for(int i = 0; i < 8; i++) {
			if(i == 5) {
				g2.setColor(Color.RED);
			} else {
				g2.setColor(Color.BLACK);
			}
			g2.rotate(Math.toRadians(45*(i+1)));
			drawLine(g2);
			g2.rotate(Math.toRadians(-45*(i+1)));
		}
		g2.translate(-50, -50);

		g2.draw(ellipse);
		
		g2.translate(50, 50);
		
		GradientPaint gradient2 = new GradientPaint(0,0,new Color(16, 75, 169),50, 0,new Color(102, 11, 171), false);
		g2.setPaint(gradient2);
		BasicStroke dashed = new BasicStroke(10.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(dashed);
		
		Area area;
		if(this.wind.getVelocity() < 0.14) {
			Ellipse2D point = new Ellipse2D.Double(-6,-6,12,12);
			area = new Area(point);
		} else {
			if(this.wind.getVelocity() < 1.5) {
				double size = 0.5;
				g2.scale(size, size);
			} else if(this.wind.getVelocity() < 30.0) {
				double size = 0.5+((0.5/28.5)*(this.wind.getVelocity()-1.5));
				g2.scale(size, size);
			} else if(this.wind.getVelocity() < 50.0){
				double size = 1.0 +((0.1/20)*(this.wind.getVelocity()-30));
				g2.scale(size, size);
			} else {
				double size = 1.1;
				g2.scale(size, size);
			}
			GeneralPath roundShape = new GeneralPath();
			roundShape.moveTo(0, 0);
			roundShape.lineTo(0, -25);
			roundShape.lineTo(50, 0);
			roundShape.lineTo(0, 25);
			roundShape.lineTo(0, 5);
			roundShape.lineTo(-50, 5);
			roundShape.lineTo(-50, -5);
			roundShape.lineTo(0, -5);
			roundShape.closePath();
			area = new Area(roundShape);
			g2.scale(1, 1);
		}
		
		g2.rotate(Math.toRadians(-this.wind.getAzimuth()));
		//Rectangle2D point = new Rectangle2D.Double(50,0,1,1);
		g2.fill(area);
		g2.setStroke(new BasicStroke(3.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		g2.setColor(Color.BLACK);
		g2.draw(area);
		g2.rotate(Math.toRadians(this.wind.getAzimuth()));
		g2.translate(-50, -50);
	}
	
	/**
	 * Vykresleni cary
	 * @param g2 Graphics2D
	 */
	public void drawLine(Graphics2D g2) {
		Line2D line = new Line2D.Double(0,0,50,0);
		g2.draw(line);
	}
	
}
