package cz.pavelzelenka.game;

import java.awt.Color;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

import javax.swing.JFileChooser;

/**
 * Nacteni dat mapy
 * @author Pavel Zelenka A16B0176P
 * @version 2017-03-05
 */

public class TerrainFileHandler {

	public int[][] terrain;
	public int columns, rows, deltaX, deltaY, shooterX, shooterY, targetX, targetY;
	
	public void loadTerFile() {
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Otevřít soubor terénu");
		fc.addChoosableFileFilter(new TerrainFilter());
		fc.setAcceptAllFileFilterUsed(false);
		int returnVal = fc.showOpenDialog(Game.frame);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			loadTerFile(file.getAbsolutePath());
		} else {
			System.out.println("Načítání souboru zrušeno!");
			generateData();
		}
	}
	
	/**
	 * Nacteni vstupniho souboru a nastaveni atributu tridy
	 * @param fileName nazev souboru
	 */
	public void loadTerFile(String fileName) {
		System.out.println("Načítání souboru terénu " + fileName);
		File file = new File(fileName);
		DataInputStream input = null;
		try{
			input = new DataInputStream(new FileInputStream(file));
			this.columns = input.readInt();
			this.rows = input.readInt();
			this.deltaX = input.readInt();
			this.deltaY = input.readInt();
			this.shooterX = input.readInt();
			this.shooterY = input.readInt();
			this.targetX = input.readInt();
			this.targetY = input.readInt();
			
			// Nacteni nadmorskych vysek na jednotlivych pozici
			terrain = new int[rows][columns];
			for (int y = 0; y < rows; y++) {
				for (int x = 0; x < columns; x++) {
					terrain[y][x] = input.readInt();
				}
			}
			
			// Vytvoreni instance terenu, strelce a cile
			Game.game.terrain = new Terrain(terrain, deltaX, deltaY);
			Game.game.shooter = new NamedPosition(shooterX*convertMMtoM(deltaX), shooterY*convertMMtoM(deltaY), 5, Color.RED, NamedPosition.PositionType.SHOOTER);
			Game.game.target = new NamedPosition(targetX*convertMMtoM(deltaX), targetY*convertMMtoM(deltaY), 5, Color.BLUE, NamedPosition.PositionType.TARGET);
		
		} catch (FileNotFoundException e) {
			System.err.println("Nepovedlo se otevřít vstupní soubor. Zadaný soubor neexistuje!");
			Game.exit();
		} catch(OutOfMemoryError e) {
			System.err.println("Nepovedlo se otevřít vstupní soubor. Zadaný soubor není správným souborem terénu!");
			Game.exit();
		} catch(Exception e) {
			System.err.println("Nepovedlo se otevřít vstupní soubor.");
			Game.exit();
		} finally {
			try {
				if (input != null) {
					input.close();		
				}
			} catch (IOException e) {
				System.err.println("Nepovedlo se otevřít vstupní soubor.");
				Game.exit();
			}
		}
	}
	
	/**
	 * Vygeneruje data a nastavi atributy tridy
	 */
	public void generateData() {
		Random random = new Random();
		this.columns = 20;
		this.rows = 20;
		this.deltaY = 5000*(1+random.nextInt(10));
		this.deltaX = (int)(((deltaY*rows)/columns));
		this.shooterX = random.nextInt(columns);
		this.shooterY = random.nextInt(rows);
		this.targetX = random.nextInt(columns);
		this.targetY = random.nextInt(rows);
		
		terrain = RandomTerrain.getRandomTerrain(rows, columns, 50);
		
		// Vytvoreni instance terenu, strelce a cile
		Game.game.terrain = new Terrain(terrain, deltaX, deltaY);
		Game.game.shooter = new NamedPosition(shooterX*convertMMtoM(deltaX), shooterY*convertMMtoM(deltaY), 5, Color.RED, NamedPosition.PositionType.SHOOTER);
		Game.game.target = new NamedPosition(targetX*convertMMtoM(deltaX), targetY*convertMMtoM(deltaY), 5, Color.BLUE, NamedPosition.PositionType.TARGET);
		
	}
	
	/**
	 * Vypsani nactenych dat na konzoli
	 */
	public void printData() {
		System.out.println("velikost mapy: " + columns + "x" + rows + " bunek");
		System.out.println("rozestupy: " + deltaX + "x" + deltaY + " mm");		
		
		if(rows < 10) {
			for (int y = 0; y < rows; y++) {
				System.out.println(Arrays.toString(terrain[y]));
			}
		}
		
		System.out.println("sirka mapy: " + Game.game.terrain.getWidthInM() + " m");
		System.out.println("vyska mapy: " + Game.game.terrain.getHeightInM() + " m");
	}
	
	/**
	 * Prevede milimetry na metry
	 * @param mm milimetry
	 * @return metry
	 */
	public double convertMMtoM(double mm) {
		return mm/1000.0;
	}
	
	/**
	 * Prevede metry na milimetry
	 * @param m metry
	 * @return milimetry
	 */
	public double convertMtoMM(double m) {
		return m*1000.0;
	}
	
}
