package cz.pavelzelenka.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JPanel;

/**
 * Zobrazeni mapy
 * @author Pavel Zelenka A16B0176P
 * @version 2017-03-05
 */

public class GamePanel extends JPanel {

	private static final long serialVersionUID = -4240590701742805914L;

	public Timer timer = new Timer();
	
	public Terrain terrain;
	public NamedPosition shooter, target, hitSpot;
	public Wind wind;
	public Trajectory trajectory;
	public Radar radar;
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		Graphics2D g2 = (Graphics2D)g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.translate(10, 10);		// odsazeni od okraju okna - posunuti poc pozice do 10,10
		
		double scale = getScale();
		if (terrain != null) {
			terrain.draw(g2, scale);
		}
		if (trajectory != null) {
			trajectory.draw(g2, scale);
		}
		if (hitSpot != null) {
			hitSpot.draw(g2, scale);
		}
		if (shooter != null) {
			shooter.draw(g2, scale);
		}
		if (target != null) {
			target.draw(g2, scale);
		}
		AffineTransform transform = g2.getTransform();
		if (wind != null) {
			g2.translate(terrain.getWidthInM()*scale+5, 0);
			radar.draw(g2, scale);
		}
		g2.setTransform(transform);
	}
	
	/** 
	 * Vrati meritko mapy
	 * @return meritko
	 */
	public double getScale() {
		double width = ((double)this.getWidth()-20-135)/terrain.getWidthInM();
		double height = ((double)this.getHeight()-20)/terrain.getHeightInM();
		if(width < height) {
			return width;
		} else { 
			return height;
		}
	}
	
	/** 
	 * Akce casovace
	 */
	public void addActions() {
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				wind.generateParams();
				repaint();
			}
		}, 100, 100);
	}
	
}
