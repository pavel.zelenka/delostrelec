package cz.pavelzelenka.game;

import java.util.Random;

/**
 * Generator nahodneho terenu
 * @author Pavel Zelenka A16B0176P
 * @version 2017-04-08
 */
public class RandomTerrain {

	/**
	 * Vrchol
	 * @author Pavel Zelenka A16B0176P
	 * @version 2017-03-05
	 */
	private class Hill {
		public int elevation;
		public int area;
		public int x;
		public int y;
		
		/**
		 * Vrchol
		 * @param x souradnice X
		 * @param y souradnice Y
		 * @param elevation vyska v nejvyssim bode
		 * @param area velikost oblasti
		 */
		public Hill(int x, int y, int elevation, int area) {
			this.x = x;
			this.y = y;
			this.elevation = elevation;
			this.area = area;
		}
	}
	
	private int[] terrain; 
	private int columns;
	private int rows;
	
	/**
	 * Nahodny teren
	 * @param rows radky
	 * @param columns sloupce
	 * @param peaks pocet vrcholu
	 */
	private RandomTerrain(int rows, int columns, int peaks) {
		this.rows = rows;
		this.columns = columns;
		this.terrain = new int[rows*columns];
		for(int i = 0; i < terrain.length; i++) {
			terrain[i] = 100000;
		}
		
		Random random = new Random();
		for(int i = 0; i<peaks; i++) {
			Hill hill = new Hill(random.nextInt(rows-1),random.nextInt(columns-1),random.nextInt(400000),3+random.nextInt(12));
			addHill(hill);
		}
	}
	
	/**
	 * Pridat vrchol do terenu
	 * @param hill vrchol
	 */
	private void addHill(Hill hill) {
		Random random = new Random();
		for(int i = 0; i < terrain.length; i++) {
			
			int d = i/this.rows;
			int m = i%this.columns;
			
			double distance = getDistance(hill.y, hill.x, d,m);
			if(distance <= hill.area) {
				double cut = hill.elevation/hill.area;
				if(d == hill.y) {
					if(m == hill.x) {
						terrain[i] =  Math.max((int)(hill.elevation),terrain[i]);
					}
				}
				for(int z = 0; z<hill.area; z++) {
					if(d == hill.y) {
						if(m == hill.x-z || m == hill.x+z) {
							terrain[i] =  Math.max((int)(hill.elevation-distance*cut),terrain[i]);
						}
					}
					for(int y = 0; y<hill.area; y++) {
						if(d == (hill.y-z+y) || d == (hill.y+z-y)) {
							if(m == hill.x-z || m == hill.x+z) {
								double elevation = hill.elevation-distance*cut+distance*cut*random.nextDouble()*0.33;
								terrain[i] =  Math.max((int)(elevation),terrain[i]);
							}
						}
						if(d == (hill.y-z) || d == (hill.y+z)) {
							if(m == hill.x-z+y || m == hill.x+z-y) {
								double elevation = hill.elevation-distance*cut+distance*cut*random.nextDouble()*0.33;
								terrain[i] =  Math.max((int)(elevation),terrain[i]);
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Vrati vzdalenost mezi body
	 * @param x1 souradnice X prvniho bodu
	 * @param y1 souradnice Y prvniho bodu
	 * @param x2 souradnice X druheho bodu
	 * @param y2 souradnice Y druheho bodu
	 * @return vzdalenost
	 */
	public double getDistance(int x1, int y1, int x2, int y2) {
		double sx = x2 - x1;
		double sy = y2 - y1;
		double dv = Math.sqrt(sx*sx + sy*sy);
		return dv;
	}
	
	/**
	 * Vrati matici terenu
	 * @return matice terenu
	 */
	private int[][] generateTerrain3D() {
		int c = 0;
		int[][] terrain3D = new int[rows][columns];
		for (int y = 0; y < rows; y++) {
			for (int x = 0; x < columns; x++) {
				terrain3D[y][x] = terrain[c];
				c++;
			}
		}
		return terrain3D;
	}
	
	/**
	 * Vrati nahodny teren
	 * @return nahodny teren
	 */
	public static int[][] getRandomTerrain() {
		Random random = new Random();
		return getRandomTerrain(20,20,random.nextInt(100));	
	}
	
	/**
	 * Vrati nahodny teren
	 * @param rows pocet radku
	 * @param columns pocet sloupcu
	 * @param peaks pocet vrcholu
	 * @return nahodny teren
	 */
	public static int[][] getRandomTerrain(int rows, int columns, int peaks) {
		RandomTerrain randomTerrain = new RandomTerrain(rows,columns,peaks);
		return randomTerrain.generateTerrain3D();	
	}
	
}
