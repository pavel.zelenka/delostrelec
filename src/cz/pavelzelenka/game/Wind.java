package cz.pavelzelenka.game;

import java.util.Random;

/**
 * Trida generujici parametry vetru
 * @author Pavel Zelenka A16B0176P
 * @version 2017-03-25
 */
public class Wind {

	public static double convertMpsToKmph(double mps) {
		return (3.6*mps);
	}
	
	private double velocity, maxVelocity;	// rychlost
	private double azimuth; 				// smer
	
	// zmeny rychlosti vetru
	private double velocityBefore;
	private double velocityAfter;
	private int velocityStep; 
	private int velocityStepsCount; 
	
	// zmeny proudu vetru
	private double azimuthBefore;
	private double azimuthAfter;
	private int azimuthStep; 
	private int azimuthStepsCount;
	
	/**
	 * Vytvori instanci vetru
	 * @param maxVelocity maximaln rychlost vetru
	 */
	public Wind(double maxVelocity) {
		this.maxVelocity = Math.abs(maxVelocity);
		this.velocity = maxVelocity/2;
		this.azimuth = 0.0;
		
		this.velocityAfter = this.velocity;
		this.azimuthAfter = this.azimuth;
	}
	
	/**
	 * Vygenerovani rychlosti a smeru vetru
	 */
	public void generateParams() {	
		Random r = new Random();
		
		// zmeny rychlosti vetru
		if(velocity == velocityAfter) {
			velocityStep = 0;
			double minVelocityChange = -5.5;
			double maxVelocityChange = 5;
			double randomVelocity = minVelocityChange + (maxVelocityChange - minVelocityChange) * r.nextDouble();
			double suggested = Math.abs(velocity + randomVelocity);
			this.velocityAfter = (suggested<maxVelocity?suggested:maxVelocity);
			this.velocityBefore = velocity;
			this.velocityStepsCount = 500;
		} else {
			double step = (velocityAfter-velocityBefore)/velocityStepsCount;
			this.velocity = velocity + step;
			this.velocityStep++;
			if(velocityStep > velocityStepsCount) {
				this.velocity = velocityAfter;
			}
		}
		
		// zmeny proudu vetru
		if(azimuth == azimuthAfter) {
			azimuthStep = 0;
			double minAzimuthChange = -45;
			double maxAzimuthChange = 45;
			double randomAzimuth = minAzimuthChange + (maxAzimuthChange - minAzimuthChange) * r.nextDouble();
			this.azimuthAfter = azimuth + randomAzimuth;
			this.azimuthBefore = azimuth;
			this.azimuthStepsCount = 500;
		} else {
			double step = (azimuthAfter-azimuthBefore)/azimuthStepsCount;
			this.azimuth = azimuth + step;
			this.azimuthStep++;
			if(azimuthStep > azimuthStepsCount) {
				this.azimuth = azimuthAfter;
			}
		}
	}
	
	/**
	 * Vrati aktualni rychlost vetru.
	 * @return aktualni rychlost vetru
	 */
	public double getVelocity() {
		return velocity;
	}

	/**
	 * Vrati maximalni rychlost vetru.
	 * @return maximalni rychlost vetru
	 */
	public double getMaxVelocity() {
		return maxVelocity;
	}

	/**
	 * Vrati aktualni smer vetru.
	 * @return aktualni smer vetru
	 */
	public double getAzimuth() {
		return azimuth;
	}
	
	/**
	 * Vrati slozku X vektoru rychlosti vetru.
	 * @return slozka X vektoru rychlosti vetru
	 */
	public double getVx() {
		double magnitude = this.velocity;
		double direction = Math.toRadians(this.azimuth);
		double xComponent = magnitude * Math.cos(-direction);	// x = delka vektoru * sin(smer) Proc to mam jinak? :-/
		return xComponent;
	}
	
	/**
	 * Vrati slozku Y vektoru rychlosti vetru.
	 * @return slozka Y vektoru rychlosti vetru
	 */
	public double getVy() {
		double magnitude = this.velocity;
		double direction = Math.toRadians(this.azimuth);
		double yComponent = magnitude * Math.sin(-direction);	// y = delka vektoru * cos(smer)
		return yComponent;
	}
	
	/**
	 * Vrati slozku Z vektoru rychlosti vetru.
	 * @return slozka Z vektoru rychlosti vetru
	 */
	public double getVz() {
		return 0D;
	}

	/**
	 * Nastavi maximalni rychlsot vetru v m/s.
	 * @param maxVelocity maximalni rychlsot vetru v m/s
	 */
	public void setMaxVelocity(double maxVelocity) {
		this.maxVelocity = maxVelocity;
		if(velocityAfter > maxVelocity) {
			setVelocityAfter(maxVelocity);
		}
	}
	
	/** 
	 * Nastavi rychlost vetru v dalsim kroku.
	 * Je-li pozadovana rychlost mensi nez 0, nastavi se rychlost na 0.
	 * Je-li pozadovana rychlost vetsi nez maximalni povolena rychlost vetru, nastavi se na maximalni rychlost vetru.
	 * @param velocityAfter pozadovana rychlost vetru v m/s
	 */
	public void setVelocityAfter(double velocityAfter) {
		if(velocityAfter > maxVelocity) {
			velocityAfter = maxVelocity;
		} else if(velocityAfter < 0) {
			this.velocityAfter = 0.0;
		} else {
			this.velocityAfter = velocityAfter;
		}
		velocityBefore = velocity;
		velocityStepsCount = 100;
		velocityStep = 0;
	}

	/**
	 * Nastaveni smeru vetru v dalsim kroku.
	 * @param azimuthAfter pozadovany smer vetru ve stupnich
	 */
	public void setAzimuthAfter(double azimuthAfter) {
		this.azimuthAfter = azimuthAfter;
		this.azimuthBefore = azimuth;
		azimuthStep = 0;
		azimuthStepsCount = 100;
	}
	
}
