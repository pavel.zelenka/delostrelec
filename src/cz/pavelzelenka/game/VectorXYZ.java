package cz.pavelzelenka.game;

/**
 * Trida uchovavajici bod v trojrozmernem prostoru
 * @author Pavel Zelenka A16B0176P
 * @version 2017-03-26
 */
public class VectorXYZ {

	public final double x,y,z;
	
	/**
	 * Vytvori instanci bodu v trojrozmernem prostoru
	 * @param x souradnice bodu na ose X
	 * @param y souradnice bodu na ose Y
	 * @param z souradnice bodu na ose Z
	 */
	public VectorXYZ(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * Vrati vzdusnou vzdalenost
	 * @param position pozice instance
	 * @return vzdusna vzdalenost
	 */
	public double getDistance(NamedPosition position) {
		double x1 = this.x;
		double y1 = this.y;
		double x2 = position.getX();
		double y2 = position.getY();
		return countDistance(x1,y1,x2,y2);
	}
	
	/**
	 * Vrati vzdusnou vzdalenost
	 * @param position pozice instance
	 * @return vzdusna vzdalenost
	 */
	public double getDistance(VectorXYZ position) {
		double x1 = this.x;
		double y1 = this.y;
		double x2 = position.x;
		double y2 = position.y;
		return countDistance(x1,y1,x2,y2);
	}
	
	/**
	 * Vypocet vzdalenosti
	 * @param x1 pozice
	 * @param y1 pozice
	 * @param x2 pozice
	 * @param y2 pozice
	 * @return vzdalenost
	 */
	private double countDistance(double x1, double y1, double x2, double y2) {
		// smerovy vektor
		double sx = x2 - x1;
		double sy = y2 - y1;
		// delka smeroveho vektoru
		double dv = Math.sqrt(sx*sx + sy*sy);
		return dv;
	}
	
	@Override
	public String toString() {
		return "VectorXYZ [X="+x+";Y="+y+";Z="+z+"]";
	}
}
