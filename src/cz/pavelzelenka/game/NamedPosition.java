package cz.pavelzelenka.game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * Pozice objektu na mape
 * @author Pavel Zelenka A16B0176P
 * @version 2017-03-05
 */

public class NamedPosition {

	private final static String IMG_TARGET = "img/target.png";
	private final static String IMG_SHOOTER = "img/shooter.png";
	
	public enum PositionType {SHOOTER, TARGET, HIT_SPOT}
	
	/** pozice v metrech */
	private double x, y;
	
	/** velikost v metrech */
	private double size;
	
	/** barva objektu */
	private Color color;
	
	/** typ pozice */
	private PositionType positionType;
	
	/** obrazek */
	private Image image;
	
	/**
	 * Vytvori instanci tridy NamedPosition
	 * @param x pozice na ose X (v metrech)
	 * @param y pozice na ose Y (v metrech)
	 * @param size veliksot
	 * @param color barva
	 */
	public NamedPosition(double x, double y, double size, Color color, PositionType positionType) {
		this.x = x;
		this.y = y;
		this.size = size;
		this.color = color;
		this.positionType = positionType;
		if(positionType.equals(PositionType.TARGET)) {
			image = readImage(IMG_TARGET);
		} else if(positionType.equals(PositionType.SHOOTER)) {
			image = readImage(IMG_SHOOTER);
		}
	}
	
	/**
	 * Vrati vzdusnou vzdalenost mezi 2 instancemi
	 * @param position pozice instance
	 * @return vzdusna vzdalenost
	 */
	public double getDistance(NamedPosition position) {
		double x1 = this.x;
		double y1 = this.y;
		double x2 = position.x;
		double y2 = position.y;
		// smerovy vektor
		double sx = x2 - x1;
		double sy = y2 - y1;
		// delka smeroveho vektoru
		double dv = Math.sqrt(sx*sx + sy*sy);
		return dv;
	}
	
	/**
	 * Metoda pro zobrazeni grafickeho kontextu
	 * @param g2 Graphics2D
	 * @param scale meritko
	 */
	public void draw(Graphics2D g2, double scale) {
		g2.setColor(color);
		int xs = (int)(x*scale);
		int ys = (int)(y*scale);
		if(positionType.equals(PositionType.HIT_SPOT)) {
			g2.fillOval(xs - (int)(30*scale), ys - (int)(30*scale), (int)(60*scale), (int)(60*scale));
		} else if(positionType.equals(PositionType.TARGET) && image != null) {
			g2.drawImage(image, xs-5, ys-5, 10, 10, null);
		} else if(positionType.equals(PositionType.SHOOTER) && image != null) {
			g2.drawImage(image, xs-5, ys-5, 10, 10, null);
		} else {
			g2.drawLine(xs, ys, xs+5, ys);
			g2.drawLine(xs, ys, xs, ys+5);
			g2.drawLine(xs, ys, xs-5, ys);
			g2.drawLine(xs, ys, xs, ys-5);
		}
	}
	
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		if(color != null) {
			this.color = color;
		}
	}

	public PositionType getPositionType() {
		return positionType;
	}

	public void setPositionType(PositionType positionType) {
		if(positionType != null) {
			this.positionType = positionType;
		}
	}
	
	/**
	 * Nacte obrazek.
	 * @param path cesta k obrazku
	 * @return obrazek
	 */
	private Image readImage(String path) {
		Image image = null;
		try {
			image = ImageIO.read(new File(path));
		} catch (IOException e) {
			System.err.println("Nepovedlo se nacist soubor: " + path); 
		}
		return image;
	}
	
}
