package cz.pavelzelenka.game;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * Filtr souboru terenu
 * @author Pavel Zelenka A16B0176P
 * @version 2017-03-04
 */
public class TerrainFilter extends FileFilter {

    public final static String terrain = "ter";
    
    /**
     * Vrati priponu souboru
     * @param f soubor
     * @return pripona
     */
    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }
	
    /**
     * Filtr souboru
     */
    @Override
	public boolean accept(File f) {
	    if (f.isDirectory()) {
	        return true;
	    }

	    String extension = getExtension(f);
	    if (extension != null) {
	        if (extension.equals(terrain)) {
	                return true;
	        } else {
	            return false;
	        }
	    }

	    return false;
	}

    /**
     * Popis souboru
     */
	@Override
	public String getDescription() {
		return "Soubor terénu (*.ter)";
	}
	
}
