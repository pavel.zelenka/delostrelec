package cz.pavelzelenka.game;

import java.util.Scanner;

import javax.swing.JFrame;


/**
 * Hlavni trida hry
 * @author Pavel Zelenka A16B0176P
 * @version 2017-03-27
 */

public class Game {

	public static final String title = "Pavel Zelenka A16B0176P";
	
	public static final Scanner scanner = new Scanner(System.in);
	
	public static final JFrame frame = new JFrame();
	public static final GamePanel gamePanel = new GamePanel();
	
	public static Game game = new Game();
	public static GameWindow gameWindow;
	
	public GraphController graphController;
	public TerrainFileHandler terrainFile;
	public Terrain terrain;
	public NamedPosition shooter;
	public NamedPosition target;
	public ShootingCalculator shootingCalculator;
	public Wind wind;
	public Radar radar;
	
	public static void main(String[] args) {
		game.start();
	}
	
	/** Spusti hru */
	public void start() {
		loadTerrain();					// nacteni terenu
		
		init();
		
		makeWindow();					// vytvoreni okna
	}
	
	/** Obsluha nacteni terenu */
	public void loadTerrain() {
		terrainFile = new TerrainFileHandler();
		terrainFile.loadTerFile();
	}
	
	public String getGameObjectInfo() {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("pozice střelce: " + Game.game.shooter.getX() + " x " + Game.game.shooter.getY() + " m\n");
		stringBuffer.append("pozice cíle: " + Game.game.target.getX() + " x " + Game.game.target.getY() + " m\n");
		stringBuffer.append("výška střelce: " + terrain.getAltitudeInM(shooter.getX(), shooter.getY()) + " m\n");
		stringBuffer.append("výška cíle: " + terrain.getAltitudeInM(target.getX(), target.getY()) + " m\n");
		stringBuffer.append("vzdálenost střelce od cíle: " + String.format("%.3f", Game.game.shooter.getDistance(Game.game.target)) + " m\n");
		return stringBuffer.toString();
	}
	
	/** Vypise podrobnosti hernich objektu*/
	public void printGameObjectInfo() {
		System.out.println(getGameObjectInfo());
	}
	
	public void init() {
		initShootingCalculator();		// inicializace vypoctu strely
		initWind(10.5);					// inicializace vetru (40m/s = 144km/h, 30m/s = 108km/h, 20m/s = 72km/h, 10m/s = 36km/h) 
		initRadar();					// inicializace radaru
		initGamePanel();				// inicializace herni plochy
		initGraphController();			// inicializace grafu
	}
	
	/** Inicializace vypoctu strely*/
	public void initShootingCalculator() {
		shootingCalculator = new ShootingCalculator(shooter, target);
		shootingCalculator.terrain = this.terrain;
	}
	
	/** Inicializace vetru */
	public void initWind(double maxVelocity) {
		wind = new Wind(maxVelocity);
		shootingCalculator.wind = this.wind;
	}
	
	/** Inicializace radaru */
	public void initRadar() {
		radar = new Radar(this, gamePanel, this.terrain, this.wind);
	}
	
	/** Inicializace herni plochy */
	public void initGamePanel() {
		gamePanel.terrain = this.terrain;
		gamePanel.shooter = this.shooter;
		gamePanel.target = this.target;
		gamePanel.wind = this.wind;
		gamePanel.radar = this.radar;
		gamePanel.addActions();
	}
	
	public void initGraphController() {
		graphController = new GraphController(this);
	}
	
	public void setTrajectory(Trajectory trajectory) {
		gamePanel.trajectory = trajectory;
	}
	
	/** Vysledek strelby
	 * @return 1 = zasah, -1 = vedle, 0 = mimo mapu
	 */
	public int getResult() {
		if(shootingCalculator.hitSpot == null) {
			return 0;
		} else if (shootingCalculator.hitSpot.getDistance(target) <= 30) {
			return 1;
		} else {
			return -1;
		}
	}
	
	/**
	 * Zmenit mapu
	 * @param fromFile otevrit dialog?
	 */
	public void changeMap(boolean fromFile) {
		if(fromFile) {
			loadTerrain();
		} else {
			terrainFile.generateData();
		}
		init();
		gamePanel.hitSpot = null;
		gamePanel.trajectory = null;
	}
	
	/** Zmeni vlastnosti vetru */
	public void setCalmWind() {
		this.wind.setMaxVelocity(0.2);
		this.wind.setVelocityAfter(0.01);
		System.out.println("Nyni je bezvetri!");
	}
	
	/** Zmeni vlastnosti vetru */
	public void setLightAirWind() {
		this.wind.setMaxVelocity(1.5);
		this.wind.setVelocityAfter(1.25);
		System.out.println("Nyni je vanek!");
	}
	
	/** Zmeni vlastnosti vetru */
	public void setGentleBreezeWind() {
		this.wind.setMaxVelocity(5.4);
		this.wind.setVelocityAfter(4.4);
		System.out.println("Nyni je mirny vitr!");
	}
	
	/** Zmeni vlastnosti vetru */
	public void setStrongBreezeWind() {
		this.wind.setMaxVelocity(13.8);
		this.wind.setVelocityAfter(11.5);
		System.out.println("Nyni je silny vitr!");
	}
	
	/** Zmeni vlastnosti vetru */
	public void setHighWind() {
		this.wind.setMaxVelocity(17.1);
		this.wind.setVelocityAfter(15.25);
		System.out.println("Nyni je prudky vitr!");
	}
	
	/** Zmeni vlastnosti vetru */
	public void setGaleWind() {
		this.wind.setMaxVelocity(20.7);
		this.wind.setVelocityAfter(18.5);
		System.out.println("Nyni je bourlivy vitr!");
	}
	
	/** Zmeni vlastnosti vetru */
	public void setStrongGaleWind() {
		this.wind.setMaxVelocity(24.4);
		this.wind.setVelocityAfter(22.5);
		System.out.println("Nyni je vichrice!");
	}
	
	/** Zmeni vlastnosti vetru */
	public void setStormWind() {
		this.wind.setMaxVelocity(28.4);
		this.wind.setVelocityAfter(26.5);
		System.out.println("Nyni je silna vichrice!");
	}
	
	/** Zmeni vlastnosti vetru */
	public void setViolentStormWind() {
		this.wind.setMaxVelocity(32.6);
		this.wind.setVelocityAfter(30.5);
		System.out.println("Nyni je mohutna vichrice!");
	}
	
	/** Zmeni vlastnosti vetru */
	public void setHurricaneWind() {
		this.wind.setMaxVelocity(69.9);
		this.wind.setVelocityAfter(39.5);
		System.out.println("Nyni je orkan!");
	}
	
	/** Zmeni smer vetru */
	public void setSouthWind() {
		this.wind.setAzimuthAfter(90);
		System.out.println("Nyni vitr vane z jihu!");
	}
	
	/** Zmeni smer vetru */
	public void setNorthWind() {
		this.wind.setAzimuthAfter(-90);
		System.out.println("Nyni vitr vane ze severu!");
	}
	
	/** Zmeni smer vetru */
	public void setEastWind() {
		this.wind.setAzimuthAfter(180);
		System.out.println("Nyni vitr vane od vychodu!");
	}
	
	/** Zmeni smer vetru */
	public void setWestWind() {
		this.wind.setAzimuthAfter(0);
		System.out.println("Nyni vitr vane ze zapadu!");
	}
	
	/** Zmeni gravitaci Zeme */
	public void setGZ(Double gz) {
		shootingCalculator.gz = gz;
	}
	
	/** Zobrazi okno aplikace */
	
	public static void makeWindow() {
		gameWindow = GameWindow.getInstance(null, game);
	}

	/** Ukonceni */
	public static void exit() {
		System.exit(0);
	}
	
}
