package cz.pavelzelenka.game;

import java.awt.Color;

import cz.pavelzelenka.game.NamedPosition.PositionType;

/**
 * Vypocet strely
 * @author Pavel Zelenka A16B0176P
 * @version 2017-03-05
 */

public class ShootingCalculator {

	public double gx, gy, gz, b, deltaT;
	public NamedPosition shooter, target, hitSpot;
	public Wind wind;
	public Trajectory trajectory = null;
	public Terrain terrain;
	
	/**
	 * Vytvori instanci tridy ShootingCalculator
	 * @param shooter strelec
	 * @param target cil
	 */
	public ShootingCalculator(NamedPosition shooter, NamedPosition target) {
		this.shooter = shooter;
		this.target = target;
		
		this.gx = 0.0;
		this.gy = 0.0;
		this.gz = 10;		// gravitacni zrychleni (vychozi = 10)
		this.b = 0.05;		// koef. vlivu vetru / odporu vzduchu (vychozi = 0.05)
		this.deltaT = 0.01;		// casovy krok (vychozi = 0.01)
	}
	
	/**
	 * Vypocet strely
	 * @param azimuth
	 * @param elevation
	 * @param startV
	 */
	public void shoot(double azimuth, double elevation, double startV) {
		this.trajectory = new Trajectory();
		Game.game.setTrajectory(trajectory);
		
		double shooterZ = terrain.getAltitudeInM(shooter.getX(), shooter.getY());
		double theta = Math.toRadians(elevation);
		double kappa = Math.toRadians(-azimuth);
		
		VectorXYZ firstPosition = new VectorXYZ(
				shooter.getX(),
				shooter.getY(),
				shooterZ);
		
		VectorXYZ firstShootVelocity = new VectorXYZ(
				startV * Math.cos(theta) * Math.cos(kappa),
				startV * Math.cos(theta) * Math.sin(kappa),
				startV * Math.sin(theta));
		
		trajectory.add(firstPosition);
		
		VectorXYZ lastShootVelocity = firstShootVelocity;
		VectorXYZ lastPosition = firstPosition;
		VectorXYZ secondLastPosition = firstPosition;
		
		boolean valid = true;
		
		while(terrain.getAltitudeInM(lastPosition.x, lastPosition.y) <= (lastPosition.z)) {
			VectorXYZ newShootVelocity = new VectorXYZ(
					lastShootVelocity.x+0.0*this.gx*deltaT+(wind.getVx()-lastShootVelocity.x)*b*deltaT,
					lastShootVelocity.y+0.0*this.gy*deltaT+(wind.getVy()-lastShootVelocity.y)*b*deltaT,
					lastShootVelocity.z-1.0*this.gz*deltaT+(wind.getVz()-lastShootVelocity.z)*b*deltaT);
						
			VectorXYZ newPosition = new VectorXYZ(
					lastPosition.x+newShootVelocity.x*deltaT,
					lastPosition.y+newShootVelocity.y*deltaT,
					lastPosition.z+newShootVelocity.z*deltaT);
			
			if(!terrain.isValidLocation(newPosition.x, newPosition.y)) {
				valid = false;
				break;
			}
			
			secondLastPosition = lastPosition;
			trajectory.add(newPosition);
			lastShootVelocity = newShootVelocity;
			lastPosition = newPosition;
		}
		
		if(valid == true) {
			VectorXYZ bisected = bisection(lastPosition, secondLastPosition);
			this.hitSpot = new NamedPosition(bisected.x, bisected.y, 10, Color.ORANGE, PositionType.HIT_SPOT);
		} else {		
			this.hitSpot = null;
		}
		
		
		Game.gamePanel.hitSpot = this.hitSpot;
	}
	
	/**
	 * Metoda vrati bod dopadu streli na zem s presnosti 1 mm
	 * @param last poslodni bod
	 * @param secondLast predposledni bod
	 * @return bod dopadu streli na zem ziskany pulenim intervalu
	 */
	private VectorXYZ bisection(VectorXYZ last, VectorXYZ secondLast) {
		VectorXYZ top = secondLast;
		VectorXYZ bottom = last;
		VectorXYZ bisected = new VectorXYZ((top.x+bottom.x)/(2.0), (top.y+bottom.y)/(2.0), (top.z+bottom.z)/(2.0));
		double altitude = terrain.getAltitudeInM(bisected.x, bisected.y);
		int attempt = 0;
		while(altitude != bisected.z) {
			if(Math.abs((altitude-bisected.z)) < 0.001) break;
			if(attempt == 100) break;
			if(altitude > bisected.z) {
				bottom = bisected;
				bisected = new VectorXYZ((top.x+bisected.x)/(2.0), (top.y+bisected.y)/(2.0), (top.z+bisected.z)/(2.0));
			} else {
				top = bisected;
				bisected = new VectorXYZ((bisected.x+bottom.x)/(2.0), (bisected.y+bottom.y)/(2.0), (bisected.z+bottom.z)/(2.0));
			}
			altitude = terrain.getAltitudeInM(bisected.x, bisected.y);
			attempt++;
		}
		return bisected;
	}
	
	/**
	 * Vypocet strely
	 * @param azimuth
	 * @param distance
	 */
	public void shoot(double azimuth, double distance) {
		double degree = azimuth%360;
		int mark = (azimuth > 0 ? 1 : -1);
		if(mark == 1 && degree>180) {
			mark = -1;
		} else if(mark == -1 && Math.abs(degree)>180) {
			mark = 1;
			degree = Math.abs(degree);
		}
		double alfa = degree+90;
		double beta = 90.0;
		double gama = 180.0-(alfa+beta);
		double a = distance;
		double b = a * Math.sin(Math.toRadians(alfa));
		double c = mark * Math.sqrt(Math.pow(a, 2)-Math.pow(b, 2));
		this.hitSpot = new NamedPosition(Game.game.shooter.getX()+b, Game.game.shooter.getY()-c, 10, Color.ORANGE, PositionType.HIT_SPOT);
		Game.gamePanel.hitSpot = this.hitSpot;
	}
	
	/**
	 * Vrati dostrel v metrech
	 * dle vzorce d=(v^2/g)*sin(2*theta)
	 * @param elevation elevance
	 * @param startV pocatecni rychlost
	 * @return dostrel v metrech
	 */
	public double getRange(double elevation, double startV) {
		double d = 0;				// distance
		double v = startV;			// velocity
		double g = this.gz;			// gravitation acceleration
		double theta = elevation;	// elevation
		d = ((v*v)/g)*Math.sin(Math.toRadians(2*theta));
		return d;
		
	}
	
	/**
	 * Vrati dostrel v metrech
	 * @param namedPosition pozice
	 * @param elevation elevance
	 * @param startV pocatecni rychlost
	 * @return dostrel v metrech
	 */
	public double getRange(NamedPosition namedPosition, double elevation, double startV) {
		double d = 0;				// distance
		double v = startV;			// velocity
		double g = this.gz;			// gravitation acceleration
		double theta = Math.toRadians(elevation);	// elevation
		double y0 = terrain.getAltitudeInM(namedPosition.getX(), namedPosition.getY());
		d = ((v*Math.cos(theta))/g)*(v*Math.sin(theta)+Math.sqrt(v*v*Math.sin(theta)*Math.sin(theta)+2*g*y0));
		return d;
	}
	
	/**
	 * Vrati vysku strely v metrech
	 * dle vzorce y = y0 + x*tan(theta) - ((g*x^2) / (2*(v*cos(theta)^2)))
	 * @param namedPosition pozice
	 * @param elevation elevance
	 * @param startV pocatecni rychlost
	 * @param distance vzdalenost
	 * @return vysku strely v metrech
	 */
	public double getAltitude(NamedPosition namedPosition, double elevation, double startV, double distance) {
		double y = 0;				// altitude
		double v = startV;			// velocity
		double g = this.gz;			// gravitation acceleration
		double y0 = terrain.getAltitudeInM(namedPosition.getX(), namedPosition.getY());	// start altitude
		double x = distance;		// distance
		double theta = Math.toRadians(elevation);		// elevation
		double xtanth = x*Math.tan(theta);			// x * tan(theta)
		double vcosth = v*Math.cos(theta);			// v * cos(theta)
		y = y0 + xtanth - ( (g*x*x) / (2*vcosth*vcosth) );
		return y;
	}
	
	public Trajectory getTrajectory() {
		return trajectory;
	}

	public void setTrajectory(Trajectory trajectory) {
		this.trajectory = trajectory;
	}
	
}
