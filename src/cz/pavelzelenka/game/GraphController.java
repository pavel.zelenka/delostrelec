package cz.pavelzelenka.game;

import java.awt.Color;
import java.awt.GradientPaint;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYAreaRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleInsets;

/**
 * Kontroler grafu
 * @author Pavel Zelenka A16B0176P
 * @version 2017-05-13
 */

public class GraphController {

	enum Mode {
		Range,
		TerrainProfile;
	}
	
	private static final long serialVersionUID = 1L;
	
	public Game game;
	public Mode mode;
	
	private ChartPanel drawingArea;
	private JFreeChart chart;
	
	private double azimuth = 0;
	private double elevation = 10;
	private double velocity = 200;
	
	private List<VectorXYZ> terrainPoints;
	
	public GraphController(Game game) {
		this.game = game;
		showTerrainProfileChart();
	}

	public void showRangeChart() {
		this.mode = Mode.Range;
		makeRangeChart();
		this.drawingArea = new ChartPanel(chart);
	}
	
	public void showTerrainProfileChart() {
		this.mode = Mode.TerrainProfile;
		makeTerrainChart();
		this.drawingArea = new ChartPanel(chart);
	}
	
	public void makeRangeChart() {
		XYSeries data = generateRangeData();
		XYSeriesCollection ds = new XYSeriesCollection();
		ds.addSeries(data);
		chart = ChartFactory.createXYLineChart("Range",
				"Distance [m]", "Start velocity [m/s]",
				ds, PlotOrientation.VERTICAL,
				false, true, false);
		XYPlot plot = (XYPlot) chart.getPlot();
        plot.setAxisOffset(new RectangleInsets(0, 0, 0, 0));
        plot.setBackgroundPaint(Color.white);
        plot.setRangeGridlinePaint(Color.black);
        plot.setForegroundAlpha(0.9f);
	}
	
	
	public void makeTerrainChart() {
		generateTerrainPoints();
		XYSeries data1 = generateTerrainData();
		XYSeries data2 = generateTrajectoryData();
		XYSeriesCollection ds = new XYSeriesCollection();
		ds.addSeries(data1);
		ds.addSeries(data2);
		chart = ChartFactory.createXYLineChart("Terrain Profile",
				"Distance [m]", "Altitude [m]",
				ds, PlotOrientation.VERTICAL,
				false, true, false);
		XYPlot plot = (XYPlot) chart.getPlot();
		
        XYAreaRenderer renderer = new XYAreaRenderer();
        plot.setRenderer(0, renderer);
        renderer.setSeriesPaint(1, new GradientPaint(0.0f, 0.0f, new Color(237, 28, 35), 0.0f, 0.0f, new Color(154, 9, 13)));  
        renderer.setOutline(true);
        renderer.setSeriesOutlinePaint(1, new Color(154, 9, 13));
       
        renderer.setSeriesPaint(0, new GradientPaint(0.0f, 0.0f, new Color(166, 67, 0), 0.0f, 0.0f, new Color(191, 106, 48)));  
        renderer.setSeriesOutlinePaint(0, new Color(154, 9, 13));
        
        plot.setBackgroundPaint(Color.white);
        plot.setRangeGridlinePaint(Color.black);
        plot.setForegroundAlpha(1.0f);

	}

	public void setParams(double a, double e, double v) {
		this.azimuth = a;
		this.elevation = e;
		this.velocity = v;
		if(mode.equals(Mode.TerrainProfile)) {
			makeTerrainChart();
		} else {
			makeRangeChart();
		}
		this.drawingArea.setChart(chart);
	}
	
	/** Graf dostrelu */
	public XYSeries generateRangeData() {
		XYSeries data = new XYSeries("Range");
		
		double v = (velocity*2.0)/100.0;
		double e = elevation;
		if(v<0.1D) v=0.1D;
		if(e<0.1D) e=0.1D;
		for(int i = 0; i<100; i++) {
			double range = game.shootingCalculator.getRange(e, i*v);
			data.add(range, i*v);
		}		
		return data;
	}	
	
	/** Data terenu slouzici jako podklad pro tvorbu dat grafu */
	public void generateTerrainPoints() {
		terrainPoints = new ArrayList<VectorXYZ>();
		
		double size = game.terrain.getWidthInM();
		double distance = size/200.0;
		double x = game.shooter.getX();
		double y = game.shooter.getY();
		double z = game.terrain.getAltitudeInM(x, y);
		terrainPoints.add(new VectorXYZ(x,y,z));
		
		while(game.terrain.isValidLocation(x, y)) {
			x += distance * Math.cos(Math.toRadians(-azimuth));
			y += distance * Math.sin(Math.toRadians(-azimuth));
			if(game.terrain.isValidLocation(x, y)) {
				try{
					z = game.terrain.getAltitudeInM(x, y);
					terrainPoints.add(new VectorXYZ(x,y,z));
				} catch(Exception e) {
					break;
				}
			}
		}
	}
	
	/** Graf pudorysu terenu */
	public XYSeries generateTerrainData() {
		XYSeries data = new XYSeries("Terrain");

		double size = game.terrain.getWidthInM();
		double distance = size/200.0;
		
		for(int i = 0; i<terrainPoints.size(); i++) {
			VectorXYZ vector = terrainPoints.get(i);
			data.add((i*distance),vector.z);
		}

		return data;
	}	

	
	/** Graf trajektorie */
	public XYSeries generateTrajectoryData() {
		XYSeries data = new XYSeries("Trajectory");
	
		if(terrainPoints != null && !terrainPoints.isEmpty()) {
			for(int i = 0; i<terrainPoints.size(); i++) {
				VectorXYZ vector = terrainPoints.get(i);
				double distance = vector.getDistance(game.shooter);
				double altitude = game.shootingCalculator.getAltitude(game.shooter, elevation, velocity, distance);
				if(altitude < vector.z) {
					if(altitude > 0) {
						data.add(distance,altitude);
					} else {
						data.add(distance,0);
					}
					break;
				}
				data.add(distance,altitude);
			}
		} else {
			data.add(0,0);
		}

		return data;
	}	

	public ChartPanel getPanel() {
		return drawingArea;
	}

}
