package cz.pavelzelenka.game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;
import java.util.ArrayList;

/**
 * Trida uchovavajici trajektorii strely
 * @author Pavel Zelenka A16B0176P
 * @version 2017-03-26
 */
public class Trajectory {

	public ArrayList<VectorXYZ> trajectory;
	
	/**
	 * Vytvori instanci trajektorie
	 */
	public Trajectory() {
		trajectory = new ArrayList<VectorXYZ>();
	}
	
	/**
	 * Prida bod trajektorie
	 * @param x souradnice bodu na ose X
	 * @param y souradnice bodu na ose Y
	 * @param z souradnice bodu na ose Z
	 */
	public void add(double x, double y, double z) {
		add(new VectorXYZ(x,y,z));
	}
	
	/**
	 * Prida bod trajektorie
	 * @param vector vektor
	 */
	public void add(VectorXYZ vector) {
		trajectory.add(vector);
	}
	
	/**
	 * Vrati pocet bodu trajektorie
	 * @return pocet bodu trajektorie
	 */
	public int size() {
		return trajectory.size();
	}

	/**
	 * Metoda pro zobrazeni grafickeho kontextu
	 * @param g2 Graphics2D
	 * @param scale meritko
	 */
	public void draw(Graphics2D g2, double scale) {
		if(!trajectory.isEmpty()) {
			g2.setColor(Color.YELLOW);
			double[][] points = new double[trajectory.size()][2];
			
			for(int i = 0; i < trajectory.size(); i++) {
				VectorXYZ position = trajectory.get(i);
				points[i][0] = (double)(position.x*scale);
				points[i][1] = (double)(position.y*scale);
			}
			
	        GeneralPath path = new GeneralPath();
	        path.moveTo(points[0][0], points[0][1]);
	        for (int k = 1; k < points.length; k++)
	        	path.lineTo(points[k][0], points[k][1]);
	        g2.draw(path);
		}
	}
	
	/**
	 * Vrati souradnice bodu na ose X
	 * @param index poradi bodu v trajektorii
	 * @return souradnice bodu na ose X
	 */
	public double getX(int index) {
		return trajectory.get(index).x;
	}
	
	/**
	 * Vrati souradnice bodu na ose Y
	 * @param index poradi bodu v trajektorii
	 * @return souradnice bodu na ose Y
	 */
	public double getY(int index) {
		return trajectory.get(index).y;
	}
	
	/**
	 * Vrati souradnice bodu na ose Z
	 * @param index poradi bodu v trajektorii
	 * @return souradnice bodu na ose Z
	 */
	public double getZ(int index) {
		return trajectory.get(index).z;
	}
	
	/**
	 * Vrati souradnice bodu
	 * @param index poradi bodu v trajektorii
	 * @return souradnice bodu
	 */
	public VectorXYZ get(int index) {
		return trajectory.get(index);
	}
	
}
