package cz.pavelzelenka.game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

/**
 * Teren
 * @author Pavel Zelenka A16B0176P
 * @version 2017-03-25
 */

public class Terrain {

	public int terrain[][];					// matice mapy ([radky][sloupce])
	public int deltaXInMM, deltaYInMM;		// vzdalenosti mezi prvky matice
	private BufferedImage terrainImage;		// textura terenu
	
	/**
	 * Vytvori instanci tridy Terrain
	 * @param terrain pole vysek
	 * @param deltaX rozestupy sloupcu
	 * @param deltaY rozestupy radku
	 */
	public Terrain(int[][] terrain, int deltaX, int deltaY) {
		this.terrain = terrain;
		this.deltaXInMM = deltaX;
		this.deltaYInMM = deltaY;
		makeImage();
	}
	
	/**
	 * Vrati nadmorskou vysku v danem bode
	 * @param x souradnice bodu na ose X
	 * @param y souradnice bodu na ose Y
	 * @return nadmorska vyska v bode
	 */
	public double getAltitudeInM(double x, double y) {		
		int l = new Integer(Math.max(terrain.length, terrain[0].length)).toString().length();
		double d = Math.pow(10.0, l);	// ci je mozno nastavit na pevno napr. 1000.0

		double xp = x/convertMMtoM(deltaXInMM);
		double yp = y/convertMMtoM(deltaYInMM);
		
		int x1 = ((int)(xp*d))/((int)(d));
		int y1 = ((int)(yp*d))/((int)(d));
		
		int x2 = (int) Math.ceil((xp*d)/d);
		int y2 = (int) Math.ceil((yp*d)/d);
		
		if(x1 <= 0) {
			x1 = 0;
		}
		if(y1 <= 0) {
			y1 = 0;
		}
		if(x2 >= terrain[0].length) {
			x2 = terrain[0].length - 1;
		}
		if(y2 >= terrain.length) {
			y2 = terrain.length - 1;
		}
		
		int q11 = (int)(convertMMtoM((terrain[y1][x1])));
		int q21 = (int)(convertMMtoM((terrain[y1][x2])));
		int q12 = (int)(convertMMtoM((terrain[y2][x1])));
		int q22 = (int)(convertMMtoM((terrain[y2][x2])));
		
		double p1 = (((x2-xp)*(y2-yp))/((x2-x1)*(y2-y1)))*q11;
		double p2 = (((xp-x1)*(y2-yp))/((x2-x1)*(y2-y1)))*q21;
		double p3 = (((x2-xp)*(yp-y1))/((x2-x1)*(y2-y1)))*q12;
		double p4 = (((xp-x1)*(yp-y1))/((x2-x1)*(y2-y1)))*q22;
		
		Double p = p1 + p2 + p3 + p4;

		if(p.equals(Double.NaN)) {
			Double qxl = (((xp-x1)*(q22-q12))/(x2-x1))+q12;
			if(!qxl.equals(Double.NaN)) {
				return qxl;
			}
			Double qyl = (((yp-y1)*(q22-q21))/(y2-y1))+q21;
			if(!qyl.equals(Double.NaN)) {
				return qyl;
			}
			return q11;
		} else {
			return p;
		}
		
	}
	
	/**
	 * Metoda pro zobrazeni grafickeho kontextu
	 * @param g2 Graphics2D
	 * @param scale meritko
	 */
	public void draw(Graphics2D g2, double scale) {
		g2.setColor(Color.WHITE);
		Rectangle2D rectangle = new Rectangle2D.Double();
		rectangle.setFrame(0, 0, (int)(getWidthInM()*scale), (int)(getHeightInM()*scale));
		g2.fill(rectangle);
		
		AffineTransform affineTransform = AffineTransform.getScaleInstance(
				(getWidthInM()*scale)/((double)terrainImage.getWidth()), 
				(getHeightInM()*scale)/((double)terrainImage.getHeight())
				);
		
		AffineTransformOp op = new AffineTransformOp(affineTransform, AffineTransformOp.TYPE_BICUBIC);
		
		g2.drawImage(terrainImage, op, 0, 0);
		
		g2.setColor(Color.BLACK);
		g2.draw(rectangle);
		g2.setClip(rectangle);
		
	}
	
	/**
	 * Vrati sirku v metrech
	 * @return sirka v metrech
	 */
	public double getWidthInM() {
		if(terrain.length > 0) {
			return (terrain[0].length)*convertMMtoM(deltaXInMM);
		}
		return 0.0;
	}
	
	/** 
	 * Vrati vysku v metrech
	 * @return vyska v metrech
	 */
	public double getHeightInM() {
		return (terrain.length)*convertMMtoM(deltaYInMM);
	}
	
	/**
	 * Prevede milimetry na metry
	 * @param mm milimetry
	 * @return metry
	 */
	public double convertMMtoM(double mm) {
		return mm/1000.0;
	}
	
	/**
	 * Prevede metry na milimetry
	 * @param m metry
	 * @return milimetry
	 */
	public double convertMtoMM(double m) {
		return m*1000.0;
	}
	
	/**
	 * Vrati, zda-li se jedna o platnou souradnici uvnitr terenu
	 * @param x souradnice bodu na ose X
	 * @param y souradnice bodu na ose Y
	 * @return Vrati true, je-li souradnice uvnitr terenu
	 */
	public boolean isValidLocation(double x, double y) {
		if(x >= 0 && x <= getWidthInM()) {
			if(y >= 0 && y <= getHeightInM()) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Vytvoreni textury mapy
	 */
	private void makeImage() {
		int width = terrain[0].length;	// pocet sloupcu matice terenu
		int height = terrain.length;	// pocet radku matice terenu
		
		int min = Integer.MAX_VALUE;	// nejnizsi nadmorska vyska terenu
		int max = Integer.MIN_VALUE;	// nejvyssi nadmorska vyska terenu
		
		for(int i = 0; i < height; i++) {
			for(int j = 0; j < width; j++) {
				int value = terrain[i][j];
				if(max < value) {
					max = value;
				}
				if(min > value) {
					min = value;
				}
			}
		}
		
		this.terrainImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = terrainImage.createGraphics();
		//g2.dispose();
		
		for(int i = 0; i < height; i++) {
			for(int j = 0; j < width; j++) {
				g2.setColor(getColor(terrain[i][j], min, max));
				g2.fillRect(j, i, 1, 1);
			}
		}
	}
	
	/** 
	 * Vrati barvu dle nadmorske vysky
	 * @param value nadmorska vyska
	 * @param min nejnizsi nadmorska vyska terenu
	 * @param max nejvyssi nadmorska vyska terenu
	 * @return barva dle nadmorske vysky
	 */
	private Color getColor(int value, int min, int max) { 
		int difference = max-min;
		int component = 128;
		int deduction = value-min;
		
		if(difference != 0) {
			double coefficient = 255.0/difference;
			component = (int)(coefficient*deduction);
		}
		
		if(component > 255) component = 255;
		if(component < 0) component = 0;
		
		return new Color(component,component,component);
	}
	
}
